const projects = {
	"regentec": {
		"name": "Regentec",
		"path": "regentec",
		"colorMap": {
			"main": {
				"theme": "dark",
				"backgroundColor": "#1E2027"
			},
			"sub": {
				"theme": "light",
				"backgroundColor": "#EFEFE8"
			}
		},
		"category": "ui/ux design, branding, motion graphic",
		"thumbnail": "/assets/projects/regentec/regentec_thum.jpg",
		"main": {
			"background": "",
			"image": "/assets/projects/regentec/regentec_main_small.png",
			"theme": "dark"
		},
		"link": [
			{ "name": "Visit Website", "url": "http://lansandkim.com/regentec/" },
			{ "name": "Download Brand Guidline", "url": "/assets/projects/regentec/RegenTec_Brand_Guide.pdf" }
		],
		"description": "The goal was to redesign the logo of existing brand and make a brand guideline. RegenTec is a cutting-edge technology company handling a renewable energy. With newest technology plasma pyrolysis, they transfer trash into a clean energy. I focused on conveying a difficult technology idea to easily understandable animation using website and motion graphic. I used Adobe Muse for building website. ",
		"images": [
			{	
				type: 'image',
				theme: 'light',
				imageUrl: '/assets/projects/regentec/regentec_img_1.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'image',
				theme: 'dark',
				imageUrl: '/assets/projects/regentec/regentec_img_2.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'image',
				theme: 'light',
				imageUrl: '/assets/projects/regentec/regentec_posterimg_3.png',
				imageTag: '',
				fullwidth: false,
				style: {
					backgroundColor: "#EFEFE8",
					padding: '100px'
				}
			},
			{
				type: 'video',
				theme: 'dark',
				imageUrl: {
					pathJPG: "/assets/projects/regentec/regentec_gif_4.jpg",
					pathWEBM: "/assets/projects/regentec/regentec_gif_4.webm",
					pathMP4: "/assets/projects/regentec/regentec_gif_4.mp4"
				},
				imageTag: '',
				fullwidth: false,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'vimeo',
				theme: 'dark',
				imageUrl: 'https://player.vimeo.com/video/194583871',
				imageTag: '',
				fullwidth: false,
				backgroundColor: '',
				style: {}
			},
		]
	},

	"laon": {
		"name": "Laon",
		"path": "laon",
		"colorMap": {
			"main": {
				"theme": "light",
				"backgroundColor": "#c54433"
			},
			"sub": {
				"theme": "dark",
				"backgroundColor": "#c54433"
			}
		},
		"category": "BRANDING, PACKAGING, MOTION GRAPHIC",
		"thumbnail": "/assets/projects/laon/laon_thum.jpg",
		"main": {
			"background": "/assets/projects/laon/laon_img_0.jpg",
			"image": "",
			"theme": "light"
		},
		"link": [],
		"description": "Laon is a high-end Korean dessert cafe with traditional tea and rice cake. It is designed with korean character in the logo and created a background with the pattern from it. Each pattern is inspired from korean traditional symbol as well as a color palette.",
		"images": [
			{	
				type: 'image',
				theme: 'dark',
				imageUrl: '/assets/projects/laon/laon_img_1.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'image',
				theme: 'dark',
				imageUrl: '/assets/projects/laon/laon_img_2.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'image',
				theme: 'dark',
				imageUrl: '/assets/projects/laon/laon_img_3.jpg',
				imageTag: '',
				fullwidth: true,
				style: {
					backgroundColor: "",
					padding: ''
				}
			},
			{
				type: 'vimeo',
				theme: 'dark',
				imageUrl: 'https://player.vimeo.com/video/207971723',
				imageTag: '',
				fullwidth: false,
				backgroundColor: '',
				style: {}
			},
		]
	},
	"victorias-secret": {
		"name": "Victoria's Secret",
		"path": "victorias-secret",
		"colorMap": {
			"main": {
				"theme": "dark",
				"backgroundColor": "#1b1b1b"
			},
			"sub": {
				"theme": "dark",
				"backgroundColor": "#1b1b1b"
			}
		},
		"category": "ui/ux design",
		"thumbnail": "/assets/projects/victorias-secret/vs_thum.jpg",
		"main": {
			"background": "/assets/projects/victorias-secret/vs_img_0_1.jpg",
			"image": "/assets/projects/victorias-secret/vs_main_small.png",
			"theme": "light"
		},
		"link": [
			{ "name": "Visit Website", "url": "https://www.victoriassecret.com/" },
			{ "name": "Download Presentation Deck", "url": "/assets/projects/victorias-secret/vday_deck.pdf" }
		],
		"description": "During the Victoria’s Secret summer intern, I’ve worked on a pitching of the Valentin’s day. For this project, I came up with the idea of bold independent woman who has confidence and knows what she wants, gets what she wants, owns the dating scene, or owns the bedroom scene. As for the homepage design, it continually tells story for sexy independent woman with the copy “I’m seductive, I’m adventurous, I’m fancy”.",
		"images": [
			{	
				type: 'image',
				theme: 'light',
				imageUrl: '/assets/projects/victorias-secret/vs_img_1.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'image',
				theme: 'light',
				imageUrl: '/assets/projects/victorias-secret/vs_img_2.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'image',
				theme: 'dark',
				imageUrl: '/assets/projects/victorias-secret/vs_img_3.jpg',
				imageTag: '',
				fullwidth: true,
				style: {
					backgroundColor: "",
					padding: ''
				}
			},
			{
				type: 'image',
				theme: 'dark',
				imageUrl: '/assets/projects/victorias-secret/vs_img_4.jpg',
				imageTag: '',
				fullwidth: true,
				style: {
					backgroundColor: "",
					padding: ''
				}
			},
		]
	},

	"sol-real": {
		"name": "Sol Real",
		"path": "sol-real",
		"colorMap": {
			"main": {
				"theme": "dark",
				"backgroundColor": "#231f20"
			},
			"sub": {
				"theme": "light",
				"backgroundColor": "#947a63"
			}
		},
		"category": "UI/UX DESIGN, BRANDING, PACKAGING",
		"thumbnail": "/assets/projects/sol-real/sr_thum_1.jpg",
		"main": {
			"background": "",
			"image": "/assets/projects/sol-real/sr_main_small.png",
			"theme": "dark"
		},
		"link": [
			{ "name": "Download Presentation Deck", "url": "/assets/projects/sol-real/JR_deck_final.pdf" }
		],
		"description": "Jeff Redd was client from class. Jeff Redd who is not only a musician but also an entrepreneur, requested to design his own logo and shea butter package design. From the meeting, I noticed he wanted a design to be very gender neutral, but very elegant and somehow keep his musician background in the design. So I designed the logo with a classic feel, using black and brown. Jeff Redd really liked this logo and he imagined using this logo for his upcoming fashion brand. Also designed eCommerce website using musical elements. I am proud to say my design was awarded!",
		"images": [
			{	
				type: 'image',
				theme: 'light',
				imageUrl: '/assets/projects/sol-real/sr_img_1.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'image',
				theme: 'dark',
				imageUrl: '/assets/projects/sol-real/sr_img_2.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
				{
				type: 'image',
				theme: 'dark',
				imageUrl: '/assets/projects/sol-real/sr_img_3.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'video',
				theme: 'dark',
				imageUrl: {
					pathJPG: "/assets/projects/sol-real/sr_video.png",
					pathWEBM: "/assets/projects/sol-real/sr_video.webm",
					pathMP4: "/assets/projects/sol-real/sr_video.mp4"
				},
				imageTag: '',
				fullwidth: false,
				backgroundColor: '',
				style: {}
			},
		]
	},

	"won-mc": {
		"name": "Won & McKenzie",
		"path": "won-mc",
		"colorMap": {
			"main": {
				"theme": "light ",
				"backgroundColor": "#e5d5be"
			},
			"sub": {
				"theme": "light",
				"backgroundColor": "#f0e7db"
			}
		},
		"category": "UI/UX DESIGN, BRANDING, PACKAGING, ADVERTISEMENT DESIGN",
		"thumbnail": "/assets/projects/won-mc/wm_thum.jpg",
		"main": {
			"background": "/assets/projects/won-mc/wm_img_0.jpg",
			"image": "/assets/projects/won-mc/wm_main_small.png",
			"theme": "light"
		},
		"link": [
			{ "name": "Download Presentation Deck", "url": "/assets/projects/won-mc/Jinyoung_WM_deck.pdf" }
		],
		"description": "This project is a new business start up branding including logo, stationary, label, advertising poster, brochure, canvas bag, and website. Won & McKenzie’s target customers are women at the age 20 to 35, who live at trendy upscale neighborhood Williamsburg, have a career in Manhattan. The goal was to create a luxury fashion brand for young women.",
		"images": [
			{	
				type: 'image',
				theme: 'light',
				imageUrl: '/assets/projects/won-mc/wm_img_1.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'image',
				theme: 'dark',
				imageUrl: '/assets/projects/won-mc/wm_img_2.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
            		{
				type: 'image',
				theme: 'light',
				imageUrl: '/assets/projects/won-mc/wm_img_3.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
            		{
				type: 'image',
				theme: 'light',
				imageUrl: '/assets/projects/won-mc/wm_img_4.jpg',
				imageTag: '',
				fullwidth: true,
				backgroundColor: '',
				style: {}
			},
			{
				type: 'image',
				theme: 'light',
				imageUrl: '/assets/projects/won-mc/wm_img_5.jpg',
				imageTag: '',
				fullwidth: false,
				style: {
					backgroundColor: "#f0e7db",
					padding: '100px'
				}
			},
		]
	},

}

export default projects;
