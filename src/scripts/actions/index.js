import * as types from './ActionTypes';
import projects from '../../data/project';


export function toggleMenu() {
	return {
		type: types.TOGGLE_MENU
	}
}

export function fetchProjects() {

	return {
		type: types.FETCH_PROJECTS,
		data: projects
	}

}