import React from 'react';
import { BrowserRouter as Router, Route, Link, withRouter } from 'react-router-dom';
import Header from './global/Header';
import Footer from './global/Footer';
import Home from './page/Home';
import About from './page/About';
import Portfolio from './page/Portfolio';
import Project from './page/Project';
import Contact from './page/Contact';

import TypeKit from 'typekit-shim';


class App extends React.Component {
    constructor(props) {
        super(props);
        TypeKit('pqr0vjy');
    }

    render() {
        return (
            <Router>
                <div className="container">
                  
                    <Route exact path="/about" component={Header}/>
                    <Route path="/portfolio" component={Header}/>
                    <Route exact path="/contact" component={Header}/>

                    <div className="content">
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/about" component={About}/>
                        <Route exact path="/portfolio" component={Portfolio}/>
                        <Route exact path="/portfolio/:project" component={Project}/>
                        <Route exact path="/contact" component={Contact}/>
                    </div>

                    <Route exact path="/about" component={Footer}/>

                </div>
            </Router>
        );
    }
}

export default App;