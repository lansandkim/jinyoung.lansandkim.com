import React from 'react';
import SocialModule from '../module/Social';

class Footer extends React.Component {

    render(){
        return (
            <footer className="footer">
            	<div className="footer-title">
            		<span className="title-1">GET IN TOUCH</span>
            	</div>

            	<div className="contact-info">
            		<span className="title-1">
            			<a href="mailto:jykim.7019@gmail.com">jykim.7019@gmail.com</a>
            		</span>
            		<br/><br/>
            		<span className="title-1">+1 646 673 3151</span>
            	</div>
            	
            	<SocialModule/>
            </footer>	
		);
    }
}

export default Footer;