import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Link } from 'react-router-dom';
import SocialModule from '../module/Social';
import FullScreenMenu from '../module/FullScreenMenu';
import DynamicHeaderModule from '../module/DynamicHeader';

class Header extends React.Component {

    constructor(props) {
        super(props);

        this.dynamicHeader = null;

    }

    componentDidMount() {
        // Trigger Dynamic Header Module to detect each module's theme
        this.dynamicHeader = new DynamicHeaderModule({
            wrapper: 'page',
            row: 'section',
            header: 'header',
            top: 50,
            targetMap: [
                {
                    targetClassName: 'dark', 
                    headerClassName: 'light'
                },
                {
                    targetClassName: 'light', 
                    headerClassName: 'dark'
                }
            ]
        });
    }

    render(){
        return (
            <header className="header dark">
            	<div className="logo">
                    <Link to="/">
                        <svg version="1.1" x="0px" y="0px" viewBox="0 0 206 174.2" >
                            <path className="st0" d="M200.5,163c-4.6,0-8.6-0.5-11.9-2.4c-3.4-1.9-6.6-5-9.7-9.7l-56.2-82.8l39.3-40.9c4.7-4.9,9.9-9.2,15.5-11.9
                                c5.6-2.7,10.7-4.6,15.4-4.6h5.4V0h-69.5v10.7h4.6c5.7,0,9.9,1.3,12.4,3c2.5,1.7,3.8,4.1,3.8,6.8c0,3.5-1.7,7.4-5.4,11.4
                                l-28.3,30.4c-3.1-3.1-8.9-8.4-9.5-9c-2.8-2.6-7.1-5.1-9.7-7V19c0-2.9,1.4-4.8,3.2-6.3c1.8-1.5,5.3-2,9.5-2h8.6V0H45.7v10.7h14.3
                                c4.9,0,7.9,0.4,9.9,1.9c2,1.5,2.6,3.4,2.6,6.4v26.9c-10.7,6.4-19.1,20.4-18.7,32.2c0.3,6.9,2.7,13,6.9,18.5
                                c3.1,4.1,9.1,7.9,11.8,8.8v22.1c0,11.8-2.3,21.1-7.8,27.7c-5.5,6.6-12.8,9.9-22.3,9.9c-8.4,0-15.4-2.4-21.1-7
                                c-5.8-4.7-8.6-9.9-8.6-15.6c0-2.1,0.4-3.7,1.3-4.8c0.8-1.1,1.8-1.6,3.1-1.6c0.7,0,2.1,0.3,4.1,0.9c2.4,0.8,4.4,1.1,5.7,1.1
                                c3.6,0,6.8-1.5,9.5-4.5c2.8-3,4.1-6.7,4.1-11.2c0-5.1-1.7-9.3-5-12.7c-3.4-3.3-7.6-5-12.8-5c-6.3,0-11.7,2.6-16,7.7
                                c-4.4,5.1-6.5,12-6.5,20.5c0,11.5,4.3,21.3,13,29.3c8.6,8,19.8,12,33.4,12c10,0,19-2,26.9-6c7.9-4,13.4-9.4,17.6-16.3
                                c4.1-6.8,5.7-14.5,5.7-23V95l9.6-9.7l47,68.7c0.9,1.5,1.5,2.9,1.5,4.4c0,1.6-1,2.5-3.1,3.5c-2.1,1.1-6.3,1.1-12.6,1.1h-10.4v10.7
                                H206V163H200.5z M96.5,81.4c0,0,0-7,0-7c0-1.1-1.6-3.2-2.1-3.7c-2-1.9-3.3-3.4-5.5-5c-3.7-2.5-9-4.4-13.1-1.7
                                c-1.3,0.9-3.3,1.6-3.3,2.6v29c0,0,0,0.3,0,0.5l0,0.3v0.2c0,0,0,2.2,0,2.2h0.4c-2.9-2.7-4.6-2.6-6.4-4.8c-1.8-2.2-3-5.2-3.4-8.2
                                c-1.3-9.3,4.4-20.3,12.4-23.8c6.4-2.8,13.5-0.8,20,5.6c1.7,1.7,3.4,3.5,5,5.3c0.9,1,1.8,1.9,2.7,2.9l-1.3,0.5L96.5,81.4z"/>
                        </svg>
                    </Link>
                </div>

                <div className="menu-container">
                    <button className="icon-menu" onClick={this.props.handleMenu}></button>
                    <SocialModule/>
                </div>

                <FullScreenMenu onClose={this.props.handleMenu} isActive={this.props.menuActive} />

            </header>	
		);
    }
}

const mapStateToProps = (state) => {
    return {
        menuActive: state.menu.menuActive
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleMenu: () => { dispatch(actions.toggleMenu()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
