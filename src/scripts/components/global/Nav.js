import React from 'react';
import { Link } from 'react-router-dom';

const propTypes = {
	isModalMenu: React.PropTypes.bool
};

const defaultProps = {
	isModalMenu: false
};

class Nav extends React.Component {

	constructor(props) {
		super(props);

		this.renderHomeBlock = this.renderHomeBlock.bind(this);
	}

	renderHomeBlock() {
		if( !this.props.isModalMenu ) {
			return;
		}

		return (
			<li><Link to="/" onClick={this.props.onLink}>Home</Link></li>
		);
	}

	render() {
		return (
			<nav>
				<ul className="nav">
					{this.renderHomeBlock()}
					<li><Link to="/about" onClick={this.props.onLink}>Who I am</Link></li>
					<li><Link to="/portfolio" onClick={this.props.onLink}>Portfolio</Link></li>
					<li><Link to="/contact" onClick={this.props.onLink}>Get in Touch</Link></li>
				</ul>
			</nav>
		);
	}
}

Nav.propTypes = propTypes;
Nav.defaultProps = defaultProps;

export default Nav;