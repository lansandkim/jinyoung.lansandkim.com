import React from 'react';


class DynamicHeaderModule {

	constructor(settings) {
		
		this.settings = settings;
		this.handleScroll = this.handleScroll.bind(this);
		window.addEventListener('scroll', this.handleScroll);
		this.handleScroll();
	}


	handleScroll() {
		let currentRow = null;
		let scrollTop = document.body.scrollTop;
		
		const targetContainer = document.getElementsByClassName(this.settings.wrapper)[0];		
		const rows = targetContainer.querySelectorAll(this.settings.row);
		const header = document.getElementsByClassName(this.settings.header)[0];
		
		if( rows.length ){
			Array.from(rows).map( (row) => {
				if( scrollTop >= row.offsetTop - this.settings.top ) {
					currentRow = row;
				}
			});

			this.settings.targetMap.map( (target) => {
				if ( currentRow.className.indexOf(target.targetClassName) > -1 && header.className.indexOf(target.targetClassName) > -1 ) {
					header.classList.remove(target.targetClassName);
					header.classList.add(target.headerClassName);
				}
				
			} );
		}

	}

	refresh() {
		this.handleScroll();
	}
}

export default DynamicHeaderModule;