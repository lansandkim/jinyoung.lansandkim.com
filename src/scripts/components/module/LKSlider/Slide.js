import React from 'react';
import { Link } from 'react-router-dom';

class Slide extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

		let { path, name, category, thumbnail } = this.props.slide;
		return (
			<div className="slide" style={this.props.style} onMouseOver={this.props.onHover} >
				<div className="thumb">
					<img src={thumbnail} alt={name} />
				</div>
				<div className="inner">
					<span className="subtitle-2 category">{category}</span>
					<h2 className="title-4 name">{name}</h2>
				</div>
				<div className="hover-inner">
					<div className="subtitle-2 category">{category}</div>
					<h2 className="title-4 name">{name}</h2>
					<Link className="cta light" to={"/portfolio/" + path}>View Project</Link>
				</div>
			</div>
		);
	}
}

export default Slide;