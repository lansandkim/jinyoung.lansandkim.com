import React from 'react';
import Slide from './Slide';
import {debounce} from 'throttle-debounce';

class LKSlider extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			currentIndex: 0,
			slidesPerPage: 3,
            windowWidth: window.innerWidth,
            slideWidth : window.innerWidth * 0.25,
            slideMargin: window.innerWidth * 0.0625
		}

		this.slideTo = this.slideTo.bind(this);
		this.handlePrev = this.handlePrev.bind(this);
		this.handleNext = this.handleNext.bind(this);
		this.handleResize = this. handleResize.bind(this);

	}

	componentDidMount() {
	    window.addEventListener('resize', this.handleResize);
	}

	componentWillUnmount(){
	    window.removeEventListener('resize', this.handleResize);
	}

	handleResize(e) {
		debounce(500, () => {
			this.setState({
		        windowWidth: window.innerWidth,
	            slideWidth : window.innerWidth * 0.25,
	            slideMargin: window.innerWidth * 0.0625
	        });	
		})();
	}

	slideTo(idx) {
		if(idx == -1 || idx == this.props.slides.length - this.state.slidesPerPage + 1 ) {
			return;
		}

		this.setState({
			currentIndex: idx
		});
	}

	handlePrev() {
		this.slideTo(this.state.currentIndex - 1);
	}

	handleNext() {
		this.slideTo(this.state.currentIndex + 1);
	}

	render() {

		let style = {
			containerStyle: {
				padding: `0 ${this.state.slideMargin}px`
			},
			wrapperStyle: {
				width: this.props.slides.length * (this.state.slideWidth + this.state.slideMargin),
				transform: `translateX(-${this.state.currentIndex * (this.state.slideWidth + this.state.slideMargin)}px)`
			},
			slideStyle: {
				width: this.state.slideWidth,
				margin: `0 ${this.state.slideMargin}px 0 0`
			}
		}

		return (
			<div className="lk-slider-container light" style={style.containerStyle}>
				<div className="slide-wrapper" style={style.wrapperStyle}>
				{this.props.slides.map( (slide, i) => (
					<Slide 
						key={i} 
						slide={slide} 
						style={style.slideStyle}  />
				))}
				</div>
				<div className="slide-nav-container">
					<div className="slide-btn btn-prev" onClick={this.handlePrev}></div>
					<div className="slide-btn btn-next" onClick={this.handleNext}></div>
				</div>
				
			</div>
		);
	}
}

export default LKSlider;