import React from 'react';
import FontAwesome from 'react-fontawesome';

const propTypes = {
	social: React.PropTypes.array
};

const defaultProps = {
	social: [
		{ icon: 'linkedin', to: 'https://www.linkedin.com/in/jinyoung-kim-87091485' },
		{ icon: 'instagram', to: 'https://www.instagram.com/jykim.7019/' },
		{ icon: 'pinterest-p', to: 'https://www.pinterest.com/jykim7019/' },
		{ icon: 'vimeo', to: 'https://www.vimeo.com/jinyoungkim' }
	]
};


class SocialModule extends React.Component {

	render() {
		return (
			<ul className="module module-social">
    			{this.props.social.map( (item, i) => (
    				<li key={i}>
    					<a href={item.to} target="_blank">
    						<FontAwesome name={item.icon} />
    				 	</a>
    				</li>
    			))}
    		</ul>
		);
	}
}

SocialModule.propTypes = propTypes;
SocialModule.defaultProps = defaultProps;

export default SocialModule;