import React from 'react';

const propTypes = {
	fullscreen: React.PropTypes.bool,
	autoplay: React.PropTypes.bool,
	loop: React.PropTypes.bool,
	delay: React.PropTypes.number,
	pathJPG: React.PropTypes.string,
	pathWEBM: React.PropTypes.string,
	pathMP4: React.PropTypes.string
};

const defaultProps = {
	fullscreen: false,
	autoplay: false,
	loop: false,
	delay: 0,
	pathJPG: "/assets/newyorknewyork-bg.jpg",
	pathWEBM: "/assets/newyorknewyork-bg-mid.webm",
	pathMP4: "/assets/newyorknewyork-bg-mid.mp4"
};


class VideoModule extends React.Component {
	constructor(props) {
		super(props);

		if( this.props.delay ) {
			setTimeout( ()=> {
				this.refs.video.play();
			}, this.props.delay);
		}
	}

	componentDidUpdate(_prevProps, _prevState) {
		this.refs.video.load();
	}

	render() {
		return (
			<div className={(this.props.fullscreen)?"module module-video row fullscreen": "module module-video row"}>
				<video autoPlay={this.props.autoplay}  poster={this.props.pathJPG} loop={this.props.loop} ref="video">
                    <source src={this.props.pathMP4} type={"video/mp4"} />
                    <source src={this.props.pathWEBM} type={"video/webm"} />
                    Your browser does not support the video tag. I suggest you upgrade your browser.
                </video>
			</div>
		);
	}
}

VideoModule.propTypes = propTypes;
VideoModule.defaultProps = defaultProps;

export default VideoModule;