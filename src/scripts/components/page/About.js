import React from 'react';

class About extends React.Component {
    constructor(props) {
        super(props);

        this.resumePath = "./assets/jinyoungkim_resume_2017.pdf";
        this.backgroundImagePath = "./assets/about-bg4.jpg";
    }

    render() {
        let style = {
            backgroundImage: `url(${this.backgroundImagePath})`
        }
        return (
            <div className={"page page-about"}>

                <section className="fullscreen" style={style}>
                    <div className="inner">
                          <div className="title-1">Hi my name is JIN YOUNG KIM</div>
                          <p>I'm a multidisciplinary designer currently studying at Pratt Institute in New York City. I'm specialized in web design, user experience, and digital interactive. I'm currently looking to learn and contribute my skills to the industry.</p>
                          <a className="cta" target="_blank" href={this.resumePath}>download resume</a>
                    </div>

                    <div className="position center bottom">
                        <div className="inner">
                            <span className="icon-arrow-down"></span>
                            <span className="instruction">Scroll Down</span>    
                        </div>
                        
                    </div>
                </section>

                <section className="wrapper">
                    <div className="section-header">
                          <h1 className="title-1">MY TIMEINE</h1>
                          <p>I have 2 years experience of UX/web design from digital agency, in-house digital team, and freelance.</p>
                    </div>
                    <ul className="timeline-container">
                        <li>
                            <div className="timeline-item">
                                <h2 className="title-2">FREELANCE UI/UX Designer</h2>
                                <div className="date">Jan 2016 – Now</div>
                                
                                <ul>
                                    <li>UX concept design and client presentation.</li>
                                    <li>Designed company corporate identity.</li>
                                    <li>Multiple responsive homepage redesigns including QA testing. </li>
                                    <li>Wrote CSS styling for developer. </li>
                                </ul>
                                <h4 className="title-3">clients</h4>
                                <p>Maginx, RegenTec, Jeff Reed</p>
                            </div>
                        </li>
                        <li>
                            <div className="timeline-item">
                                <h2 className="title-2">Digital design summer intern</h2>
                                <h3 className="subtitle-1">Victoria's Secret, NYC</h3>
                                <div className="date">May 2016 – August 2016</div>
                                <ul>
                                    <li>Sketch and wireframe the projects.</li>
                                    <li>UX design for proactive chat feature.</li>
                                    <li>Design the new launching sports section.</li>
                                    <li>Pitch corporate website redesign.</li>
                                    <li>Email design for semi-annual sale series and sports section launch series.</li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="timeline-item">
                                <h2 className="title-2">Pratt Institute AOS Digital Design and Interactive Media</h2>
                                <div className="date">Aug 2015 - Now</div>
                                <ul>
                                    <li>Won Poster Heroes love poster competition. </li>
                                    <li>Won class competition for client.</li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="timeline-item">
                                <h2 className="title-2">Designer</h2>
                                <h3 className="subtitle-1">One Rockwell, NYC</h3>
                                <div className="date">October 2014 – December 2015</div>
                                <ul>
                                    <li>Designed responsive websites including e-commerce, branding websites, homepage redesign, and marketing email template design.</li>
                                    <li>Produced developer friendly style guide.</li>
                                    <li>Website maintenance including assets retouching and PPT Design.</li>
                                    <li>Presentation UX concept design.</li>
                                </ul>
                                <h4 className="title-3">clients</h4>
                                <p>CoutureLab, Sam Edelman, Circus by Sam Edelman, Pam&Gela, Welden, OneRockwell, Eddi Borgo, Marc Fisher</p>
                            </div>
                        </li>
                        <li>
                            <div className="timeline-item">
                                <h2 className="title-2">Sungshin University Bachelor’s Degree, English Language & Literature</h2>
                                <div className="date">Mar 2008 - Feb 2013</div>
                            </div>
                        </li>
                    </ul>
                </section>

            </div>   
        );
    }
}


export default About;