import React from 'react';

class Contact extends React.Component {

  render() {
    return (
      <div className={"page page-contact"}>

        <div className="frame style-1">
          <div className="inner">
        
            <div className="contact-info">
              <span className="title-1">
                <a href="mailto:jykim.7019@gmail.com">jykim.7019@gmail.com</a>
              </span>
              <br/><br/>
              <span className="title-1">+1 646 673 3151</span>
            </div>

          </div>
        </div>
      </div>
    );
  }
}


export default Contact;