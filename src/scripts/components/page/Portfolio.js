import React from 'react';
import { connect } from 'react-redux';
import LKSlider from '../module/LKSlider';
import { fetchProjects } from '../../actions';


class Portfolio extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.dispatch(fetchProjects());
  }

  render() {

    this.slides = [];
    if(this.props.projects !== undefined) {
      Object.keys(this.props.projects).map((project, i)=>{
        this.slides.push(this.props.projects[project]);
      });  
    }
    
    
    return (
      <div className="page page-portfolio">
        <section className="fullscreen dark">
      	  <LKSlider slides={this.slides} />
        </section>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    projects: state.projects.data
  };
}

export default connect(mapStateToProps)(Portfolio);