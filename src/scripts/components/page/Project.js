import React from 'react';
import SocialModule from '../module/Social';
import VideoModule from '../module/Video';
import { Link } from 'react-router-dom';
import projects from '../../../data/project';

/*
TODO: get projects data using redux
*/
class Project extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			currentProject: projects[this.props.match.params.project],
			nextProject: this.getNext(this.props.match.params.project)
		}

	    this.getNext = this.getNext.bind(this);
	    this.handleClick = this.handleClick.bind(this);
	}

	getNext(path) {
		let nextProject = '';
		let count = 1;
		Object.keys(projects).map((project)=>{
	      if(path == project ) {
	      	count--;
	      } else if(count == 0) {
	      	nextProject = projects[project];
	      	count--;
	      }
	    });

	    // if next project is undefined assign it to first project
	    if(!nextProject) {
	    	nextProject = projects[Object.keys(projects)[0]];
	    }
	    return nextProject;
	}

	handleClick() {
		this.setState({
			currentProject: this.state.nextProject,
			nextProject: this.getNext(this.state.nextProject.path)
		});
		window.scrollTo(0, 0);
	}

	getComponentByType(row) {

		if(row.type == 'video') {
			return (
				<VideoModule
					autoplay={true}
					loop={true}
					pathJPG={row.imageUrl.pathJPG}
					pathWEBM={row.imageUrl.pathWEBM}
					pathMP4={row.imageUrl.pathMP4}
				/>
			);
		} else if( row.type == 'vimeo') {
			return (
				<div className="iframe-video-container">
					<iframe src={row.imageUrl} frameBorder="0" allowFullScreen></iframe>
				</div>
			);
		}

		return (
			<div className={(row.fullwidth)? 'fullwidth-image':'generalwidth-image center'}>
				<img src={row.imageUrl} alt={row.imageTag} />
			</div>
		);

	}

	render() {
		
		const {currentProject} = this.state;

		const heroStyle = {
			backgroundColor: currentProject.colorMap.main.backgroundColor
		}

		// If Hero background image is set
		if( currentProject.main.background !== undefined ) {
			heroStyle.backgroundImage = `url(${currentProject.main.background})`
			heroStyle.minHeight = '510px';
		} 

		const descStyle = {
			backgroundColor: currentProject.colorMap.sub.backgroundColor,
		}

		// Render only if main Image exist
		let mainImage = () => {};
		if(currentProject.main.image !== undefined && currentProject.main.image != "") {
			mainImage = () => (
				<div className="wrapper main-image">
					<img src={currentProject.main.image} alt={currentProject.name} />
				</div>
			);
		} else {
			descStyle.padding = "180px 0";
		}

		return (
			<div className="page page-project">

				<section className={"section-hero " + currentProject.colorMap.main.theme } style={heroStyle}>
					{mainImage()}
				</section>

				<section className={"section-project-info " + currentProject.colorMap.sub.theme} style={descStyle}>
					<div className="row wrapper">
						<div className="four columns">
							<h1 className="name title-4">{currentProject.name}</h1>
							<h2 className="category subtitle-2">{currentProject.category}</h2>

							{currentProject.link.map((link, i)=> (
								<div key={i}>
									<a key={i} className="link-1" href={link.url} target="_blank">{link.name}</a>
								</div>
							))}
							
						</div>
						<div className="eight columns">
							<p>{currentProject.description}</p>
						</div>
					</div>
				</section>

				{currentProject.images.map((row, i)=>(
					<section key={i} className={row.theme} style={row.style}>
						{this.getComponentByType(row)}						
					</section>

				))}

				<section className="light padding top-border"> 
					<Link onClick={this.handleClick} to={/portfolio/ + this.state.nextProject.path}>
						<h2 className="category subtitle-2">Next Project</h2>
						<h1 className="name title-4">{this.state.nextProject.name}</h1>
						
						<div className="position right middle">
							<span className="icon-arrow-right"></span>
						</div>
					</Link>
					
				</section>

				<section className="section-social light padding">
					<SocialModule/>
				</section>
				
			</div>
		);
	}
}


export default Project;