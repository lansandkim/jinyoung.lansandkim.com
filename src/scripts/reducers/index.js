import { combineReducers } from 'redux';
import menu from './menu';
import projects from './projects';


const reducers = combineReducers({
	menu, projects
})

export default reducers;