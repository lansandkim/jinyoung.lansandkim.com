import * as types from '../actions/ActionTypes';

const initState = {
	menuActive: false
}

export default function menuToggle(state = initState, action) {

	switch(action.type) {
		case types.TOGGLE_MENU:
			return { 
				menuActive: !state.menuActive
			};

		default:
			return state;
	}
}