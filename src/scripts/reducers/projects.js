import * as types from '../actions/ActionTypes';

const initState = {
	data: []
}

export default function ProjectReducer(state = initState, action) {

	switch(action.type) {
		case types.FETCH_PROJECTS:
			return { 
				data: action.data
			};

		default:
			return state;
	}
}

